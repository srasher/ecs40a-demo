/* program to compute and display all prime numbers < n */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define CACHE_MAX 10000

/* Check if n is prime, first using cache as a source of possible divisors.
** Cache is an ordered table of primes.  If we reach the end without exceeding
** sqrt(n), we manually try remaining possible divisors <= sqrt(n) */
int isprime(int n, int cache[], int *end)
{
	int max = sqrt(n), tmp = 1;
	while(cache < end)
	{
		tmp = *cache;
		if(tmp > max) return 1;
		
		if(n % tmp == 0) return 0;
		
		cache++;
	}
	for(tmp += 2; tmp <= max; tmp += 2)
	{
		if(n % tmp == 0) return 0;
	}
	
	return 1;
}

/* Iterate from 2 to max, checking if i is a prime, and keeping track
** of the number of primes we find.  The first CACHE_MAX primes we find
** are saved in cache. */
int checkprimes(int max, int cache[], int *len)
{
	int i = 1, count = 1, *pos = cache, *end = cache + CACHE_MAX;

	*pos = 2;
	printf("%d\n", *pos);
	pos++;

	for(i += 2; i < max; i+=2)
	{
		if(isprime(i, cache, pos))
		{
			if(pos < end)
			{
				*pos = i;
				pos++;
			}
			printf("%d\n", i);
			count++;
		}
	}
	*len = pos - cache;
	return count;
}

/* Read argument from commandline if provided, or user if not, and print
** out a running list of all primes found */
int main(int argc, char *argv[])
{
	int n, size, cache[CACHE_MAX];
	
	if(argc != 2)
	{
		char buf[20];
		
		printf("Enter a positive integer:\n");
		fgets(buf, 20, stdin);
		n = strtol(buf, NULL, 10);
	}
	else n = strtol(argv[1], NULL, 10);
	
	if(n <= 2) printf("No primes < %d\n", n);
	else
	{
		printf("Primes < %d\n", n);
		printf("-------\nTotal: %d\n", checkprimes(n, cache, &size));
	}
	
	return 0;
}
