CC = gcc

primes: primes.o
	$(CC) -lm -o primes primes.o

primes.o: primes.c
	$(CC) -O -Wall -Wno-unused-result -pedantic -c primes.c

clean:
	rm -f primes primes.o
